import time as t
import matplotlib.pyplot as plt
import numpy as np
import os
import cv2

def initMatrix(dims):
    """Make a matrix with numbers marked"""
    aa = np.zeros(dims)

    return aa

def getReflector(dims):
    aa = np.zeros(dims)
    rows = min(dims)
    length = max(dims)

    for i in range(rows):
        aa[length - i - 1, i] = 1

    return aa

def pattern01(x, y, mult):
    return (mult * (x ^ 3)) - ((6 * y) ^ 2) == 0

def pattern02(x, y, mult):
    return (mult * (x ^ 3)) - ((12 * y) ^ 2) == 0

def pattern03(x, y, mult):
    return (mult * (x ^ 3)) - ((3 * y) ^ 2) == 0

def pattern04(x, y, mult):
    return (mult * (x ^ 3)) - ((3 * y) ^ 4) == 0

def pattern05(x, y, mult):
    return (mult * (x ^ 3)) - ((6 * y) ^ 4) == 0

def pattern06(x, y, mult):
    return (mult * (x ^ 5)) - ((6 * y) ^ 4) == 0

def pattern07(x, y, mult):
    return (mult * (x ^ 5)) - ((9 * y) ^ 4) == 0

def pattern08(x, y, mult):
    return mult * (3 + x) ^ 2 * (7 - 2 * x) == y

def pattern09(x, y, mult):
    return mult * (3 + x) ^ 2 * (2 * x) == y

def pattern10(x, y, mult):
    return (mult + x) ^ 2 * (2 * x) == y

def pattern11(x, y, mult):
    return (mult + x) ^ 3 * (2 * x) == y

def pattern12(x, y, mult):
    return mult * (3 + x) ^ 3 * (2 * x) == y

def pattern13(x, y, mult):
    return mult * (3 + x) ^ 3 * (2 * x) ^ 2 == y

def pattern14(x, y, mult):
    return mult * (3 + x) ^ 4 * (2 * x) ^ 2 == y

def pattern15(x, y, mult):
    return mult * (3 + x) ^ 4 * (2 * x) ^ 3 == y

def pattern16(x, y, mult):
    return mult * (3 + x) ^ 4 * (2 * x) ^ 3 - (5 * x) ^ 2 == y

def pattern17(x, y, mult):
    return mult * ((3 + x) ^ 4 * (2 * x) ^ 3 - (5 * x) ^ 2) == y

def pattern18(x, y, mult):
    return mult * (3 + x) ^ 4 * (2 * x) ^ 3 - int(((5 * x) ^ 2) / mult) == y

def pattern19(x, y, mult):
    return (((3 + x) ^ 4) * ((2 * x) ^ 3)) - (((5 * x) ^ 2) * mult) == y

def pattern20(x, y, mult):
    return (((3 + x) ^ 4) * ((2 * x) ^ 3)) - (((5 * x) ^ 2) * mult) + (13 * x) == y

def pattern21(x, y, mult):
    return ((3 + x) ^ mult) + (13 * x) == y

def pattern22(x, y, mult):
    return ((3 + x) ^ mult) - ((mult - 5) * x) == y

def pattern23(x, y, mult):
    return ((3 * x) ^ mult) - ((3 ^ mult) * x) == y

def pattern24(x, y, mult):
    return ((3 * x) ^ mult) - ((7 ^ mult) * x) == y

def pattern25(x, y, mult):
    return ((3 * x) ^ mult) - ((11 ^ mult) * x) == y

def pattern26(x, y, mult):
    return ((3 * x) ^ mult) - (x ^ np.abs(mult - 10)) == y

def pattern27(x, y, mult):
    return mult * x == y

def pattern28(x, y, mult):
    return mult * x + 11 == y

def pattern29(x, y, mult):
    return np.abs(mult * x - 11) == y

def pattern30(x, y, mult):
    return np.abs(x - mult) == y

def pattern31(x, y, mult):
    return x ^ mult == y

def pattern32(x, y, mult):
    return (x ^ mult) + mult == y

def pattern33(x, y, mult):
    return (x ^ mult) + 3 * mult == y

def pattern34(x, y, mult):
    return (x ^ mult) ^ 2 + 3 * mult == y

def pattern35(x, y, mult):
    return (x ^ mult) + (y ^ mult) == 0

def pattern36(x, y, mult):
    return mult % (x + 1) == y

def pattern37(x, y, mult):
    return mult % (x + 11) == y

def pattern38(x, y, mult):
    return mult % (x + 11) * 3 == y

def pattern39(x, y, mult):
    return 100 * mult % (x + 11) * 3 == y

def pattern40(x, y, mult):
    return 10 * mult % (x + 11) * 3 == y

def pattern41(x, y, mult):
    return 11 * mult % (x + 11) * 3 == y

def pattern42(x, y, mult):
    return 13 * mult % (x + 11) * 3 == y

def pattern43(x, y, mult):
    return 50 + mult % (x + 11) * 3 == y

def pattern44(x, y, mult):
    return 50 + x == y + mult

def pattern45(x, y, mult):
    return 50 + x == y - mult

def pattern46(x, y, mult):
    return 50 + x == y * mult

def pattern47(x, y, mult):
    return 50 + x == y + 2 * mult

def pattern48(x, y, mult):
    return 50 + x == y ^ mult

def pattern49(x, y, mult):
    return x == y % mult

def pattern50(x, y, mult):
    return x == mult % (y + 1)

def pattern51(x, y, mult):
    return x == (mult % (y + 1)) ^ y

def pattern52(x, y, mult):
    return 50 + x == (mult % (y + 1)) ^ y

def pattern53(x, y, mult):
    return 5 * x == (mult % (y + 1)) ^ y

def pattern54(x, y, mult):
    if x % 2 == 0:
        return 15 * x == (mult % (y + 1)) ^ y
    else:
        return 5 * x == (mult % (y + 1)) ^ y

def isprime(num):
    if num < 2:
        return False
    if num == 2:
        return True

    half = int(num / 2)

    for x in range(2, half):
        if num % x == 0:
            return False
    
    return True

def pattern55(x, y, mult):
    #print('is prime ' + str(x + y + mult) + ' = ' + str(isprime(x + y + mult)))
    return isprime(x + y + mult)

def pattern56(x, y, mult):
    return isprime(x + y - mult)

def pattern57(x, y, mult):
    return isprime(x * y - mult)

def pattern58(x, y, mult):
    return isprime((x + y) ^ mult)

def pattern59(x, y, mult):
    return isprime(x ^ y ^ mult)

def pattern60(x, y, mult):
    return isprime(50 + x ^ y ^ mult)

def pattern61(x, y, mult):
    return isprime(50 * x ^ y ^ mult)

patternFunc = pattern61

def getPlot(aa, dims, mult, func):
    rows = min(dims)
    length = max(dims)

    for i in range(rows * length):
        # y is the matrix row
        # x the matrix column
        x = int(i / rows)
        y = (i % length)

        if func(x, y, mult):
             aa[x,y] += 0.1

    return aa

def saveImage(matrix, i, dpi = 100):
    fig = plt.figure(figsize=(10, 10))
    ax = plt.Axes(fig,[0,0,1,1])
    ax.set_axis_off()
    fig.add_axes(ax)
    img = ax.matshow(matrix)
    img.set_cmap('hot')
    fig.savefig(path + "/plot" + '%02d' % i, dpi=dpi)

print('generating ' + patternFunc.__name__)

now = t.time()

path = "./patterns"

if not os.path.isdir(path):
    #make dir
    os.mkdir(path)

size = 100
dims = (size, size)

# init matrices with dims to all 0
matrixi = initMatrix(dims)
matrixt = initMatrix(dims)
# init reflect to matrix with diagonal or 1 else 0
reflect = getReflector(dims)

pathOut = './figured/' + patternFunc.__name__ + '.avi'

saveImage(matrixi, 0)

for i in range(1, 20):
    # get next matrix
    nextm = getPlot(matrixi, dims, i, patternFunc)
    # add to total matrix
    matrixt = np.add(matrixt, nextm)
    # reflect matrix and add to current matrix
    matrixr = np.matmul(matrixt, reflect)
    matrixc = np.add(matrixt, matrixr)
    # rotate -90 and add
    matrixr = np.rot90(matrixc)
    matrixc = np.add(matrixc, matrixr)
    # rotate -180 and add
    matrixr = np.rot90(matrixc, 2)
    matrixc = np.add(matrixc, matrixr)

    saveImage(matrixc, i)

#plt.show()

files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

#for sorting the file names properly
files.sort(key = lambda x: x[5:-4])
files.sort()

fps = 1
frame_array = []

for i in range(len(files)):
    filename = path + "/" + files[i]
    
    #reading each files
    img = cv2.imread(filename)
    height, width, layers = img.shape
    size = (width, height)
    
    #inserting the frames into an image array
    frame_array.append(img)

files.sort(reverse=True)

for i in range(len(files)):
    filename = path + "/" + files[i]
    
    #reading each files
    img = cv2.imread(filename)
    height, width, layers = img.shape
    size = (width, height)
    
    #inserting the frames into an image array
    frame_array.append(img)

out = cv2.VideoWriter(pathOut, cv2.VideoWriter_fourcc(*'MP42'), fps, size)

for i in range(len(frame_array)):
    # writing to a image array
    out.write(frame_array[i])

out.release()

print(size)
print(t.time() - now)