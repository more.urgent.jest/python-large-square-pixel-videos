# python large square pixel videos

a python script for generating videos of large square pixels using mathematical formulas

I just run it in visual studio code via F5

uses matplotlib to generate images and cv2 to generate video from the images, currently at 1 image / frame a second

could be improved by generating images to a window instead of to disk and then generating a video from the images

I intend to create some music to go with the videos